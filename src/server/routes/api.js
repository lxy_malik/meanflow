const express = require('express');
const router = express.Router();

/* All data should be in database. e.g. MongoDB with mongoose */
const userList = [
  {
    email: "lxy.malik@gmail.com",
    mobile: "7500103535",
    name: "Lakshya",
    password: "123"
  },
  {
    email: "abc@yopmail.com",
    mobile: "75001035**",
    name: "Lakshya Malik",
    password: "123"
  },
  {
    email: "xyz@yopmail.com",
    mobile: "75001035**",
    name: "Lxy Malik",
    password: "123"
  }
];

/* POST api listing. */

router.post('/signup', (req, res) => {
  if (req.body.email && req.body.password) {
    let user = {
      name: req.body.name,
      email: req.body.email,
      mobile: req.body.mobile,
      password: req.body.password
    }

    userList.forEach((obj) => {
      if (obj.email == req.body.email) {
        res.status(200).json({
          code: 1,
          message: 'Email already exist. But creating new user for testing.',
          token: 'JsonWebToken',
          user: JSON.stringify(user)
        });
      }
    });

    userList.push(user);
    res.status(200).json({
      code: 2,
      message: 'Registered successfully!',
      token: 'JsonWebToken',
      user: JSON.stringify(user)
    });
  } else {
    res.status(200).json({
      code: 3,
      message: 'Please provide acceptable data.'
    });
  }
});


router.post('/login', (req, res) => {
  /* Using modals will help in data checking. */
  if (req.body.email && req.body.password) {
    let index = userList.map((user) => user.email).indexOf(req.body.email);
    if (index > -1) {
      if (userList[index].password == req.body.password) {
        res.status(200).json({
          code: 1,
          message: 'Logged in successfully',
          token: 'JsonWebToken',
          user: JSON.stringify(userList[index])
        });
      } else {
        res.status(200).json({
          code: 2,
          message: 'Something wrong!'
        });
      }
    } else {
      res.status(200).json({
        code: 3,
        message: 'Don\'t have account with us? Please signup.'
      });
    }

  } else {
    res.status(200).json({
      code: 2,
      message: 'Something wrong!'
    });
  }
});

/* We can implement middleware to check token */
router.post('/getAll', (req, res) => {
  if (req.body.token === 'JsonWebToken') {
    res.status(200).json({
      code: 1,
      users: userList
    });
  }
});

router.post('/deleteUser', (req, res) => {
  if (req.body.token === 'JsonWebToken') {
    if (req.body.email) {
      let index = userList.map((user) => user.email).indexOf(req.body.email);
      if (index > -1) {
        userList.splice(index, 1);
        res.status(200).json({
          code: 1,
          message: 'User deleted successfully',
          users: userList
        });
      } else {
        res.status(200).json({
          code: 2,
          message: 'Something wrong!'
        })
      }
    }
  }
})

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

module.exports = router;
