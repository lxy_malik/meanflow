import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {
  // tslint:disable-next-line:ban-types
  serverRoot: String = 'http://localhost:3000/api/';

  constructor(
    private http: HttpClient
  ) { }

  httpPost(url: string, body) {
    if (url) {
      return this.http.post(`${this.serverRoot}${url}`, body);
    }
  }

}
