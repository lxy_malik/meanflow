import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from './../http-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userData = {};
  allUserList = [];
  dropdownStatus = false;

  constructor(
    private _http: HttpServiceService,
    private _router: Router
  ) { }

  ngOnInit() {
    this.getAllUser();
    this.userData = JSON.parse(localStorage.getItem('user'));
    if (!this.userData) {
      alert('User data missing!');
    }
  }

  toggleUserList() {
    this.dropdownStatus = !this.dropdownStatus;
  }

  getAllUser() {
    const obj = {
      token: localStorage.getItem('token')
    };

    this._http.httpPost('getAll', obj).subscribe((res) => {
      this.allUserList = res['users'];
      if (this.allUserList.length === 0) {
        alert('No users to show');
      }
    });
  }

  deleteUser(email) {
    if (confirm('Are you sure?')) {
      const obj = {
        token: localStorage.getItem('token'),
        email: email
      };

      this._http.httpPost('deleteUser', obj).subscribe((res) => {
        this.allUserList = res['users'];
      });
    }
  }

  logoutUser() {
    if (confirm('Are you sure?')) {
      localStorage.clear();
      this._router.navigate(['']);
    }
  }

}
