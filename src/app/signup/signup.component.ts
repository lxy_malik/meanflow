import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from './../http-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  title = 'MeanFlow';
  body = {
    name: '',
    email: '',
    mobile: '',
    password: '',
    rePassword: ''
  };
  constructor(
    private _http: HttpServiceService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  signupSubmit() {
    if (this.body.password && this.body.email) {
      this._http.httpPost('signup', this.body).subscribe((res) => {
        if (res['code'] === 3) {
          alert(res['message']);
        } else if (res['code'] === 1) {
          // save token from response and route to profile
          alert(res['message']);
          localStorage.setItem('token', res['token']);
          localStorage.setItem('user', res['user']);
          this._router.navigate(['profile']);
        } else if (res['code'] === 2) {
          localStorage.setItem('token', res['token']);
          localStorage.setItem('user', res['user']);
          this._router.navigate(['profile']);
        }
      });
    } else {
      alert('Please provide acceptable data');
    }
  }

}
