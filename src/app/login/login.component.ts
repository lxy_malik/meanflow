import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from './../http-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  body = {
    email: '',
    password: ''
  };

  constructor(
    private _http: HttpServiceService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  loginSubmit() {
    if (this.body.password && this.body.email) {
      this._http.httpPost('login', this.body).subscribe((res) => {
        if (res['code'] === 3) {
          alert(res['message']);
          setTimeout(()=>{
            this._router.navigate(['signup']);
          });
        } else if (res['code'] === 1) {
          // save token from response and route to profile
          localStorage.setItem('token', res['token']);
          localStorage.setItem('user', res['user']);
          this._router.navigate(['profile']);
        } else if (res['code'] === 2) {
          alert(res['message']);
        }
      });
    } else {
      alert('Please provide acceptable data');
    }
  }

}
